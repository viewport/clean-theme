The Bootstrap Theme for Scroll Viewport is an example based on Twitter bootstrap.

In order to get use the Bootstrap Theme you have two options

* If you have the Atlassian SDK installed, do a atlas-cli and pi to build and
upload the theme as a plugin.

* Otherwise, start your Confluence server with Scroll Viewport and FTP enabled,
create a new top-level folder "bootstrap-theme" and upload everything under
src/main/resources (except atlassian-plugin.xml).