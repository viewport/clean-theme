Scroll Viewport
===============
Clean Theme
---------------

![Viewport Clean Theme](https://bytebucket.org/viewport/clean-theme/raw/19810454ad16f3bda9f76b96af2fb658a62658f0/img/thumbnail.png) 

The Clean Theme for Scroll Viewport is an example theme based on Twitter's bootstrap.

In order to install this theme 

1. With viewport already installed in your system enable FTP through the admin console. 
1. With any FTP client connect to the system and create a new top-level folder - "bootstrap-theme" for example.
1. Upload every file from this folder.

For more information please visit the [Scroll Viewport documentation](http://www.k15t.com/display/VPRT/Documentation). 

Features
--------

* Slideshow support
* Home / Children Pages 
* Basic Blog support


Need Help?
----------

If you have any questions please visit 

* [Viewport Documentation Pages](http://www.k15t.com/display/VPRT/Documentation)
* [Scroll Viewport Developers Group](https://groups.google.com/forum/#!forum/scroll-viewport-dev).

Copyright  
---------

K15t Software GmbH [@k15t](http://twitter.com/k15tsoftware)
Licensed under the Apache License, Version 2.0 

Third party
---------------------------------------------------

Bootstrap           MIT     Copyright (c) Twitter, Inc.

jQuery                      Copyright (c) jQuery Foundation.

Cycle2              MIT     Created by [M. Alsup](http://jquery.malsup.com/cycle2/)

Colorbox            MIT     Created by [Jack Moore](https://github.com/jackmoore/colorbox)

Highlight.js        MIT     Created by [Ivan Sagalaev](https://github.com/isagalaev/highlight.js)