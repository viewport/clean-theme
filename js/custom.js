slideshowSetup = function () {
	$('.sp-slide').addClass('col-md-12');
    
    $('.sp-slideshow').each(function (index) {

        $(this).wrap( "<div class='sp-stage'></div>" );

        var linkNext = "next"+index;
        var linkPrev = "prev"+index;

        $('.sp-stage').prepend('<div class="sp-slide-select prev-btn '+linkPrev+'">‹</div><div class="sp-slide-select next-btn '+linkNext+'">›</div>');


        $(this)
            .attr("data-cycle-pause-on-hover", "true")
            .attr("data-cycle-swipe", "true")
            .attr("data-cycle-center-vert", "true")
            .attr("data-cycle-prev","."+linkPrev)
            .attr("data-cycle-next","."+linkNext)
            .cycle();
    
    });


    $('.sp-slideshow').click(function () {
        var paused = $('sp-slideshow').is('.cycle-paused');
        if (!paused) {
            $('.sp-slideshow').cycle('resume');
        }
        else {
            $('.sp-slideshow').cycle('pause');
        }
    });
}

elementsSetup = function(){
    $('.main-content img').each(function(){
        $(this).addClass('img-responsive');
        var link = $(this).attr('src');
        var wrapper = $('<a class="containedImage"></a>').attr('href',link);
        $(this).wrap(wrapper);
    })

    $('.containedImage').colorbox();

    $('table').each(function(){
        $(this).wrap("<div class='table-responsive'></div>")
    })

}

$(document).ready(function(){
    
    elementsSetup();
    slideshowSetup();
    hljs.initHighlightingOnLoad();

})




